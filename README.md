# M1 Parallèlisme - TP3

## Instructions
1. Se situer du dossier racine du projet (celui dans lequel se trouve le README)
2. Lancer la commande `sh compileAndRun.sh`

## Addition
Avec une matrice de taille 2^24

| Nombre de blocs | Nombre de threads | Temps d'exécution (en ns) |
|-----------------|-------------------|---------------------------|
| 2^0             | 2^24              | 164                       |
| 2^1             | 2^23              | 160                       |
| 2^2             | 2^22              | 160                       |
| 2^3             | 2^21              | 160                       |
| 2^4             | 2^20              | 160                       |
| 2^5             | 2^19              | 160                       |
| 2^6             | 2^18              | 160                       |
| 2^7             | 2^17              | 168                       |
| 2^8             | 2^16              | 160                       |
| 2^9             | 2^15              | 160                       |
| 2^10            | 2^14              | 160                       |
| 2^11            | 2^13              | 160                       |
| 2^12            | 2^12              | 160                       |
| 2^13            | 2^11              | 169                       |
| 2^14            | 2^10              | 332265                    |
| 2^15            | 2^9               | 329187                    |
| 2^16            | 2^8               | 328495                    |
| 2^17            | 2^7               | 327730                    |
| 2^18            | 2^6               | 398094                    |
| 2^19            | 2^5               | 793904                    |
| 2^20            | 2^4               | 1584921                   |
| 2^21            | 2^3               | 3167577                   |
| 2^22            | 2^2               | 6335577                   |
| 2^23            | 2^1               | 12670198                  |
| 2^24            | 2^0               | 25326060                  |

## Multiplication
Avec une matrice de taille 2^20

| Nombre de blocs | Nombre de threads | Temps d'exécution (en ns) |
|-----------------|-------------------|---------------------------|
| 2^0             | 2^20              | 162                       |
| 2^1             | 2^19              | 174                       |
| 2^2             | 2^18              | 162                       |
| 2^3             | 2^17              | 162                       |
| 2^4             | 2^16              | 163                       |
| 2^5             | 2^15              | 162                       |
| 2^6             | 2^14              | 162                       |
| 2^7             | 2^13              | 163                       |
| 2^8             | 2^12              | 163                       |
| 2^9             | 2^11              | 163                       |
| 2^10            | 2^10              | 169                       |
| 2^11            | 2^9               | 7708701                   |
| 2^12            | 2^8               | 7500434                   |
| 2^13            | 2^7               | 7393353                   |
| 2^14            | 2^6               | 7312820                   |
| 2^15            | 2^5               | 7297389                   |
| 2^16            | 2^4               | 10657547                  |
| 2^17            | 2^3               | 19237842                  |
| 2^18            | 2^2               | 35364796                  |
| 2^19            | 2^1               | 64237384                  |
| 2^20            | 2^0               | 146792128                 |

## Observation
On peut remarquer que le rapport blocs/threads ne semble pas avoir d'effet jusqu'à ce que l'on atteigne un nombre trop élevé de blocs.
On peut émettre l'hypothèse que ceci serait dû à une limite matérielle.

### PELERIN Aurélien
### 2020/2021