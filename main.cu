#include <iostream>
#include <cstdlib>
#include <ctime>
#include <chrono>
#include <tuple>

void DisplayExerciceTitle(const std::string& id) {
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "----------------------------\t\tExercice " << id << "\t\t----------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
    std::cout << "--------------------------------------------------------------------------------------------" << std::endl;
}

__global__ void multiply(int* v1, int* v2, int* result, unsigned* length) {
    unsigned resultIndex = (blockIdx.x * blockDim.x) + threadIdx.x;
    unsigned v1Index = (resultIndex / *length) * (*length);
    unsigned v2Index = resultIndex % (*length);
    result[resultIndex] = 0;
    for(unsigned i = 0; i < *length; ++i) {
        result[resultIndex] += v1[v1Index] * v2[v2Index];
        v1Index += 1;
        v2Index += (*length);
    }
}

__global__ void add(int* v1, int* v2, int* result){
    unsigned index = (blockIdx.x * blockDim.x) + threadIdx.x;
    result[index] = v1[index] + v2[index];
    return;
}

void printMatrix(int* m, unsigned n) {
    unsigned length = (unsigned)std::sqrt(n);

    for(unsigned i = 0; i < n; ++i) {
        if(i % length == 0) {
            std::cout << "(";
        }
        std::cout << m[i];
        if((i+1) % length == 0) {
            std::cout << ")\n";
        } else {
            std::cout << ", ";
        }
    }
}

void Exercice3(unsigned N, unsigned blockSize) {
    unsigned threadSize = N / blockSize;
    unsigned size = sizeof(unsigned) * N;

    // CPU memory allocation
    int* m1 = new int[N];
    int* m2 = new int[N];
    int* result = new int[N];

    // Randomize data
    for(unsigned int i = 0; i < N; ++i) {
        m1[i] = rand() % 10;
        m2[i] = rand() % 10;
    }

    // GPU memory allocation
    int *d_m1, *d_m2, *d_result;
    unsigned* d_length;
    cudaMalloc((void**)&d_m1, size);
    cudaMalloc((void**)&d_m2, size);
    cudaMalloc((void**)&d_result, size);
    cudaMalloc((void**)&d_length, sizeof(unsigned));

    // Send randomized data (which is on CPU at the moment) to GPU
    cudaMemcpy(d_m1, m1, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_m2, m2, size, cudaMemcpyHostToDevice);

    // Send matrix size to GPU
    unsigned length = (unsigned)std::sqrt(N);
    cudaMemcpy(d_length, &length, sizeof(unsigned), cudaMemcpyHostToDevice);

    // Process on GPU
    unsigned sampleSize = 10000;
    float totalTime = 0.0f;

    for(unsigned i = 0; i < sampleSize; ++i) {
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        multiply<<<blockSize, threadSize>>>(d_m1, d_m2, d_result, d_length);
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        totalTime += std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count();
    }
    unsigned average = unsigned(totalTime / sampleSize);

    // Retrieve data from GPU
    cudaMemcpy(result, d_result, size, cudaMemcpyDeviceToHost);

    /*printMatrix(m1, N);
    std::cout << "\t+\n";
    printMatrix(m2, N);
    std::cout << "\t=\n";
    printMatrix(result, N);*/

    // Display benchmark
    std::cout << "--------------------------------------\n--- Multiply\n--- N: " << N << "\n--- Block size: " << blockSize << "\n--- Thread Size: " << threadSize << "\n--- Average duration: " << average << "[ns]\n--------------------------------------\n";
}

void Exercice2(unsigned N, unsigned blockSize, bool displayResults = false) {
    unsigned threadSize = N / blockSize;
    unsigned size = sizeof(unsigned) * N;

    // CPU memory allocation
    int* v1 = new int[N];
    int* v2 = new int[N];
    int* result = new int[N];

    // Randomize data
    for(unsigned i = 0; i < N; ++i) {
        v1[i] = rand() % 10;
        v2[i] = rand() % 10;
    }

    // GPU memory allocation
    int *d_v1, *d_v2, *d_result;
    cudaMalloc((void**)&d_v1, size);
    cudaMalloc((void**)&d_v2, size);
    cudaMalloc((void**)&d_result, size);

    // Send randomized data (which is on CPU at the moment) to GPU
    cudaMemcpy(d_v1, v1, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_v2, v2, size, cudaMemcpyHostToDevice);

    // Process on GPU
    unsigned sampleSize = 10000;
    float totalTime = 0.0f;

    for(unsigned i = 0; i < sampleSize; ++i) {
        std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
        add<<<blockSize, threadSize>>>(d_v1, d_v2, d_result);
        std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
        totalTime += std::chrono::duration_cast<std::chrono::nanoseconds> (end - begin).count();
    }
    unsigned average = unsigned(totalTime / sampleSize);

    // Retrieve data from GPU
    cudaMemcpy(result, d_result, size, cudaMemcpyDeviceToHost);

    // Display benchmark
    std::cout << "--------------------------------------\n--- Addition\n--- N: " << N << "\n--- Block size: " << blockSize << "\n--- Thread Size: " << threadSize << "\n--- Average duration: " << average << "[ns]\n";
    if(displayResults) {
        std::cout << "--- Results :\n--- v1\t\tv2\t\tresult" << std::endl; 
        for(unsigned int i = 0; i < N; ++i) {
            std::cout << "--- " << v1[i] << "\t\t" << v2[i] << "\t\t" << result[i] << std::endl;
        }
    }
    std::cout << "--------------------------------------\n";

    // Release data
    delete[] v1;
    delete[] v2;
    delete[] result;
    cudaFree(d_v1);
    cudaFree(d_v2);
    cudaFree(d_result);
}

int main() {
    // Random seed
    std::srand(std::time(0));

    unsigned n = (unsigned)std::pow(2, 24);
    DisplayExerciceTitle("1");

    // Addition benchmarking
    for(unsigned i = 0; i <= 24; ++i) {
        Exercice2(n, (unsigned)std::pow(2, i));
    }

    DisplayExerciceTitle("3");
    n = (unsigned)std::pow(2, 20);

    // Multiplication benchmarking
    for(unsigned i = 0; i <= 0; ++i) {
        Exercice3(n, (unsigned)std::pow(2, i));
    }
    return 0;
}